const  modelUser  = require("../Model/schema")
const  { SEED , EXPIRESIN } = require('../Config/config')
const  bcrypt = require('bcrypt')
const  jwt = require('jsonwebtoken')
const  serveResp = require('../Function/serverResp')


exports.registerUser = function(req, res) {
    const date_c = new Date()
    let date_ob = new Date(date_c);
    let date = date_ob.getDate();
    let month = date_ob.getMonth() + 1;
    let year = date_ob.getFullYear();
    const date_register = year + "-" + month + "-" + date

    const newUser = new modelUser({
        name:       req.body.name,
        email:      req.body.email,
        phone:      req.body.phone,
        password:   bcrypt.hashSync(req.body.password,10),
        age:        req.body.age,
        gender:     req.body.gender,
        hobby:      req.body.hobby,
        date:       date_register 

    })
    newUser.save(function (error, User) {
        if (error) {
            return res.status(400).json({
                ok: false,
                message: 'Algo sucedio',
                error
            })
        }
        if (!User) {
            return res.status(400).json({
                ok: false,
                message: 'No se encontro el usuario',
                error
            })
        }
        return res.status(200).json({
            ok:true,
            message:'Se registro satisfactoriamente',
            user: User
        })
        
    })
    
}

exports.sign_in = function(req, res) {
    modelUser
    .findOne({ email: req.body.email })
    .exec()
    .then(user => {
        if (user) {
            if (!bcrypt.compareSync(req.body.password, user.password)) {
                return res.status(400).json({
                    ok:false,
                    err:{
                        message: 'El usuario o la contraseña incorrectas'
                    }
                })
            } else {
                const token = jwt.sign({
                    nombre: user.nombre,
                    id: user.id,
                    email: user.email
                }, SEED , {expiresIn: EXPIRESIN})
                user.password = ':)'
                return res.json({
                    ok:true,
                    personal: user,
                    token
                })
            }
        }
    }).catch()
}

exports.allUsers = function(err,res) {
    modelUser
    .find()
    .exec()
    .then(data => serveResp(data,null,'Se encontraron los usuarios',res))
    .catch(error => serveResp(null,error,'No se encontraron usuarios',err))    
}

exports.deleteUser = function(err,res) {
    const { id } = res.body.id
    modelUser
    .findByIdAndRemove(id)
    .then(data => serveResp(data,null,'Usuario eliminado correctamente',res))
    .catch(error => serveResp(null,error,'No se elimino el usuarios',err))
}

exports.searchUser = function(req,res) {
    if (!req.body.name) {
        console.log('sin nombre');
        modelUser
        .aggregate([
            {
                $match:{
                    $and:
                    [
                        {hobby:     req.body.hobby},
                        {gender:    'F'},
                        {age:       {'$gt': 18}},
                        //{date:       new Date(req.body.fecha)}
                    ]
                }
            },
            {
                $group:{
                    _id: "$hobby",
                    data: { $push: "$$ROOT" }
    
                }
            }
        ])
        .then(data => {
            console.log(data);
            serveResp(data,null,'La busqueda es',res)
        })
    }
    else if (!req.body.hobby) {
        console.log('sin hobby');
        modelUser
        .aggregate([
            {
                $match:{
                    $and:
                    [
                        {name:      req.body.name} ,
                        {gender:    'F'},
                        {age:       {'$gt': 18}},
                        //{date:       new Date(req.body.fecha)}
                    ]
                }
            },
            {
                $group:{
                    _id: "$hobby",
                    data: { $push: "$$ROOT" }
    
                }
            }
        ])
        .then(data => {
            console.log(data);
            serveResp(data,null,'La busqueda es',res)
        })
    }else{
        console.log('ambos campos')
        modelUser
        .aggregate([
            {
                $match:{
                    $and:
                    [
                        {name:      req.body.name} ,
                        {hobby:     req.body.hobby},
                        {gender:    'F'},
                        {age:       {'$gt': 18}},
                        //{date:       new Date(req.body.fecha)}
                    ]
                }
            },
            {
                $group:{
                    _id: "$hobby",
                    data: { $push: "$$ROOT" }
    
                }
            }
        ])
        .then(data => {
            console.log(data);
            serveResp(data,null,'La busqueda es',res)
        })
    }
}