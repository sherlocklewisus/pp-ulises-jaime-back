//======================================================================
//            LIBS
//======================================================================
const http = require('http')
const express = require('express')
const mongoose = require('mongoose')
const bodyParser = require('body-parser')
//======================================================================
//            CONFIG
//======================================================================
const { PORT, URI } = require('./Config/config')
const route = require('./Router/router')

const app = express()
app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    res.header("Access-Control-Allow-Methods", "GET, POST, OPTIONS, PUT, PATCH, DELETE");
    next();
})

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

route(app)

mongoose.connect(`${URI}`, { useNewUrlParser: true, useFindAndModify: true, useCreateIndex: true, useUnifiedTopology: true })
    .then(msg => console.log("DB online"))
    .catch(error => console.log(error))
//======================================================================
//            SERVER
//======================================================================
let server = http.createServer(app)

server.listen(PORT, (err) => {
    if (err) throw new Error(err)
})
console.log(`Server on port: ${PORT}`)