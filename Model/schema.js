const mongoose = require('mongoose')

const userSchema = mongoose.Schema({
    name:       { type: String, required:[true, 'Name required'],min:3,max:60 },
    email:      { type: String, required:[true, 'Email required'],min:6,max:1024, },
    phone:      { type: Number, required:[true, 'Phone required'],maxlegth:10 },
    password:   { type: String, required:[true, 'Password required'],minlegth:6 },
    age:        { type: Number, required:[true, 'Age required'],minleght:2 },
    gender:     { type: String, required:[true, 'Gender required'],min:1 },
    hobby:      { type: String, required:[true, 'Hobby required'] },
    date:       { type: Date },
})
module.exports = mongoose.model('User',userSchema)