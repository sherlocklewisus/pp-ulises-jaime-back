const jwt = require('jsonwebtoken')
const { SEED } = require('./config')
  function verifyToken(req, res, next) {
    const token = req.header('Authorization')
    //const  token = req.header('auth-token')

    if (!token) return res.status(401).json({error: 'Acceso denegado'})

    jwt.verify(token, SEED, (error, decode)=>{
        
        if (error) {
            return res.status(401).json({
                ok:false,
                message:'Token invalido',
                errors:error
            })
        }

        req.usuario = decode.usuario
        next()
    })
    
}
module.exports = verifyToken