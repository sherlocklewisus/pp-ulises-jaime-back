module.exports = function(app){

    const userController = require('../Controller/userController')
    const verifyToken = require('../Config/auth')
//======================================================================
//            ROUTER
//======================================================================

    app.route('/sign_in').post(userController.sign_in)
    app.route('/registerUser').post(verifyToken, userController.register)
    app.route('/allUser').get(verifyToken, userController.allUsers)
    app.route('/deleteUser').delete(verifyToken, userController.deleteUser)
    app.route('/searchUser').post(verifyToken, userController.searchUser)
}